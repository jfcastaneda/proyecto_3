package api;

import java.io.IOException;

import model.data_structures.DiGraph;
import model.data_structures.LinkedList;
import model.vo.VOPeso;
import model.vo.VOPuntoCiudad;

public interface IManager {

	// ------------------------------------------------------------------------------------
		// ------------------------- M�todos --------------------------------------------------
		// ------------------------------------------------------------------------------------

		/**
		 * M�todo que carga los archivos Json.
		 * @param direccionJson
		 * @throws Exception si hay una falla durante la carga del archivo.
		 */
		public void cargarJson() throws Exception; 
		
		public int darNumeroVertices();
		
		public int darNumeroDeArcos();
		
		public VOPuntoCiudad darVerticeMasCongestionado();
		
		public String darCaminoDeMenorDistancia();
		
		public void caminosNoPeaje();
		
		public String[] darCaminosMasCortoEnTiempo();
}
