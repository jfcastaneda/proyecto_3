package controller;

import java.io.IOException;

import api.IManager;
import model.data_structures.DiGraph;
import model.data_structures.LinkedList;
import model.data_structures.Queue;

import model.logic.Manager;
import model.vo.VOPeso;
import model.vo.VOPuntoCiudad;


/**
 * Controlador de la aplicaci�n.
 * @author jf.castaneda@uniandes.edu.co
 * @author md.galvan@uniandes.edu.co
 */
public class Controller {

	// ------------------------------------------------------------------------------------
	// ------------------------- Atributos ------------------------------------------------
	// ------------------------------------------------------------------------------------

	/**
	 * Conexi�n del controlador con el modelo.
	 */
	private static IManager manager = new Manager();

	// ------------------------------------------------------------------------------------
	// ------------------------- M�todos ------------------------------------------------
	// ------------------------------------------------------------------------------------

	/**
	 * M�todo que carga los archivos Json.
	 * @param direccionJson the direccion json.
	 * @throws Exception si el cargar falla.
	 */
	public static void cargarJson() throws Exception
	{
		manager.cargarJson();
	}


	public static int E() {
		return manager.darNumeroDeArcos();
	}

	public static int V() {
		return manager.darNumeroVertices();
	}


	public static VOPuntoCiudad darVerticeMasCongestionado() {
		// TODO Auto-generated method stub
		return manager.darVerticeMasCongestionado();
	}
	
	public static String r4()
	{
		return manager.darCaminoDeMenorDistancia();
	}
	
	public static String[] r5()
	{
		return manager.darCaminosMasCortoEnTiempo();
	}
	
	public static void r6() {
		manager.caminosNoPeaje();
	}
}
