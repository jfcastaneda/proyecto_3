package model.comparators;

import java.util.Comparator;

import model.vo.VOPeso;

public class ComparadorVOArcoValorPeaje implements Comparator<VOPeso> {

	@Override
	public int compare(VOPeso arg0, VOPeso arg1) {
		if(arg0.getValorPeaje()>arg1.getValorPeaje())
			return 1;
		else if(arg0.getValorPeaje()<arg1.getValorPeaje())
			return -1;
		else
			return 0;
	}

}