package model.comparators;

import java.util.Comparator;

import model.vo.VOServicio;

/**
 * Comparador de los servicios por su distancia en millas.
 * @author jf.castaneda@uniandes.edu.co
 * @author md.galvan@uniandes.edu.co
 */
public class ComparatorVOServicioDistancia implements Comparator<VOServicio> {

	@Override
	public int compare(VOServicio o1, VOServicio o2) {
		return ((Double)o1.getDistanciaRecorridaEnMillas()).compareTo(o2.getDistanciaRecorridaEnMillas());
	}
}
