package model.comparators;

import java.util.Comparator;

import model.vo.VOServicio;

/**
 * Comparador de los servicios por su duracion en segundos.
 * @author jf.castaneda@uniandes.edu.co
 * @author md.galvan@uniandes.edu.co
 */
public class ComparatorVOServicioDuracion implements Comparator<VOServicio>{

	@Override
	public int compare(VOServicio o1, VOServicio o2) {
		return ((Integer)o1.getDuracionEnSegundos()).compareTo(o2.getDuracionEnSegundos());
	}
}
