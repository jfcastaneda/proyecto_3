package model.comparators;

import java.util.Comparator;

import model.vo.VOServicio;

/**
 * Comparador de los servicios por su fecha de inicio.
 * @author jf.castaneda@uniandes.edu.co
 * @author md.galvan@uniandes.edu.co
 */
public class ComparatorVOServicioFechaInicio implements Comparator<VOServicio> {

	@Override
	public int compare(VOServicio o1, VOServicio o2) {
		return ((Long)o1.getFechaInicio()).compareTo(o2.getFechaInicio());
	}

}
