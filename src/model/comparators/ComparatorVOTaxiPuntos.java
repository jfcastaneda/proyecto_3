package model.comparators;

import java.util.Comparator;

import model.vo.VOTaxi;

/**
 * Comparador de los taxis por sus puntos.
 * @author jf.castaneda@uniandes.edu.co
 * @author md.galvan@uniandes.edu.co
 */
public class ComparatorVOTaxiPuntos implements Comparator<VOTaxi>{

	@Override
	public int compare(VOTaxi o1, VOTaxi o2) {
		return Double.compare(o1.getPuntosTaxi(), o2.getPuntosTaxi());
	}
}
