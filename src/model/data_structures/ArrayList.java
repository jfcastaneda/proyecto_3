package model.data_structures;

import java.util.Iterator;

import sun.security.provider.VerificationProvider;

public class ArrayList<T> implements Iterable<T> {

	private int tamanio;

	private T[] array;	

	public ArrayList(){
		array = (T[]) new Object[1];
		tamanio=0;
	}

	public int size() {
		return tamanio;
	}

	public boolean isEmpty() {
		return tamanio==0;
	}


	public T get(int index) {
		if(verificarRango(index)) return array[index];
		else return null;
	}

	public boolean contains(T elem) {
		boolean rta = false;
		for (int i = 0; i < array.length && !rta; i++) {
			if(array[i].equals(elem)) {
				rta=true;
			}
		}
		return rta;
	}

	public void set(int index, T elem) {
		if(verificarRango(index)) array[index]=elem;
	}

	public void add(T elem) {
		if(tamanio==array.length) resize();
		int index = buscarPosicion();
		if(index>=0) { 
			tamanio++;
			array[index] = elem;
		}
	}

	public void add(T elem, int index) {
		if(verificarRango(index)) {
			if(tamanio==array.length) resize();
			for (int i=tamanio-1; i >= index; i--) {array[i+1] = array[i];}
			array[index] = elem;                          
			tamanio++;
		}
	}

	public T remove(int index) {
		if(verificarRango(index)) {
			T rta = array[index];
			for (int i = index; i < tamanio-1; i++){array[i] = array[i+1];}
			array[tamanio-1] = null;
			tamanio--;
			return rta;
		}else return null;
	}

	public T remove(T elem) {
		int index = 0;
		for (int i = 0; i < tamanio-1; i++){
			if(array[i+1].equals(elem)) {
				index = i+1;
			}
		}
		return remove(index);
	}



	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new IteradorArray<T>();
	}



	private class IteradorArray<T> implements Iterator<T>{

		private int j;

		@Override
		public boolean hasNext() {
			return j<tamanio;
		}

		@Override
		public T next() {
			if(j==tamanio) return null;
			else return (T) array[j++];
		}

	}

	private void resize() {
		T[] newArray = (T[]) new Object[tamanio*2];
		for (int i = 0; i < array.length; i++) {
			newArray[i] = array[i];
		}
		this.array=newArray;
	}


	private int buscarPosicion() {
		int rta=0;
		for (;rta < array.length; rta++) {
			if(array[rta]==null) {
				return rta;
			}
		}
		return -1;
	}

	private boolean verificarRango(int index) {
		return index>=0 && index<tamanio;
	}



}
