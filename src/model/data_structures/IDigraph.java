package model.data_structures;

import java.util.NoSuchElementException;


public interface IDigraph<K,V,A> {
	
	public int V();
	
	public int E();
	
	public void addVertex(K idVertex, V infoVertex) throws IllegalArgumentException;
	
	public void addEdge(K idVertexIni, K idVertexFin, A infoArc) throws IllegalArgumentException, NoSuchElementException;
	
	public V getInfoVertex(K IdVertex) throws IllegalArgumentException, NoSuchElementException;
	
	public void setInfoVertex(K idVertex, V infoVertex) throws NoSuchElementException;
	
	public A getInfoArc(K idVertex, K idVertexFin) throws NoSuchElementException,  IllegalArgumentException;
	
	public void setInforArc(K idVertexIni, K idVertexFin, A infoArc) throws NoSuchElementException,  IllegalArgumentException;
	
	public  Iterable<K> adj (K idVertex) throws IllegalArgumentException, NoSuchElementException;
	
	
}
