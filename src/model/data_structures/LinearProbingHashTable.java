package model.data_structures;

import java.util.Iterator;

public class LinearProbingHashTable<K,V> implements IHashTable<K, V>{

	private final static int CAPACIDAD_INICIAL = 10;
	private final static double FACTOR = 0.5;
	private int size; //N
	private int capacidad; //M
	private NodoTabla<K, V>[] casillas;

	public LinearProbingHashTable() {
		this(CAPACIDAD_INICIAL);
	}

	public LinearProbingHashTable(int capacidad) {
		size=0;
		this.capacidad = capacidad;
		casillas = new NodoTabla[capacidad];
		for (int i = 0; i < casillas.length; i++) {
			casillas[i] = new NodoTabla<K, V>(null, null);
		}
	}

	public int getSize() {
		return size;
	}

	public int getcapacidad() {
		return capacidad;
	}

	public NodoTabla<K, V>[] getCasillas() {
		return casillas;
	}

	public ArrayList<NodoTabla<K,V>> getArray(){
		ArrayList<NodoTabla<K,V>> rta = new ArrayList<NodoTabla<K,V>>();
		for (NodoTabla<K, V> Nodo : casillas) {
			if(Nodo.llave!=null) rta.add(Nodo);
		}
		return rta;
	}

	public boolean isEmpty() {
		return size==0;
	}

	public boolean contains(K llave) {
		if(llave==null) return false;
		else return get(llave)!=null;
	}

	private int hash(K llave) {
		return (llave.hashCode() & 0x7fffffff) % capacidad;
	}

	private void resize() {
		LinearProbingHashTable<K, V> newHT = new LinearProbingHashTable(2*capacidad);
		for (NodoTabla<K, V> Nodo : casillas) {
			newHT.put(Nodo.llave, Nodo.valor);
		}
		size = newHT.size;
		casillas = newHT.casillas;
		capacidad=newHT.capacidad;
	}


	@Override
	public void put(K llave, V valor) {
		if(llave!=null) {
			double fac = (double)size/(double)capacidad;
			if(fac>FACTOR) resize();
			int index = hash(llave);
			for(;casillas[index]!=null; index = (index+1)%capacidad) {
				if(casillas[index].llave==null) {
					casillas[index].llave=llave;
					casillas[index].valor = valor;
					size++;
					return;
				}
				else if(casillas[index].llave.equals(llave)) {
					casillas[index].valor=valor;
					return;
				}
			}
		}
	}


	@Override
	public V get(K llave) {
		if(llave==null) return null;
		for(int i = hash(llave);casillas[i]!=null; i=(i+1)%capacidad) {
			if(casillas[i].llave.equals(llave)) {
				return casillas[i].valor;
			}
		}
		return null;
	}


	@Override
	public V delete(K llave) {
		if(llave==null) return null;
		for(int i = hash(llave);casillas[i]!=null; i=(i+1)%capacidad) {
			if(casillas[i].llave.equals(llave)) {
				V valor = casillas[i].valor;
				casillas[i].valor=null;
				return valor;
			}
		}
		return null;
	}


	@Override
	public Iterator<K> keys() {
		ArrayList<K> llaves = new ArrayList<K>();
		for (NodoTabla<K, V> Nodo : casillas) {
			llaves.add(Nodo.llave);
		}
		return llaves.iterator();
	}

	public static class NodoTabla <K,V> {

		private K llave;
		private V valor;
		private NodoTabla<K, V> next;

		public NodoTabla(K llave,V valor){
			this.llave = llave;
			this.valor = valor;
		}

		public K getllave(){
			return llave;
		}

		public V getvalor(){
			return valor;
		}

		public void setllave(K llave){
			this.llave = llave;
		}

		public V setvalor(V valor){
			V old = this.valor;
			this.valor = valor;
			return old;
		}

	}

}
