package model.data_structures;

import java.util.Comparator;
import java.util.Iterator;

/**
 * Clase que representa la LinkedLista encadenada gen�rica a utilizar. <br>
 * Modificado el nombre de la estructura de LinkedLista a LinkedList.
 */
public class LinkedList<E> implements Iterable<E> {

	// -----------------------------------------------------------------
	// Atributos
	// -----------------------------------------------------------------

	/**
	 * Clase que representa a un nodo gen�rico. <br>
	 */
	@SuppressWarnings("hiding")
	private class Nodo<E> {

		// -----------------------------------------------------------------
		// Atributos
		// -----------------------------------------------------------------

		/**
		 * Objeto que se va a guardar
		 */
		private E data;

		/**
		 * Siguiente nodo en la LinkedLista encadenada
		 */
		private Nodo<E> siguiente;


		// -----------------------------------------------------------------
		// Constructor
		// -----------------------------------------------------------------

		/**
		 * Crea un nuevo nodo gen�rico. <br>
		 * <b> post: </b> El el contenido del nodo se inicializ� con los datos dados por parametro, su nodo siguiente es nulo.
		 * Crea un nuevo nodo genérico. <br>
		 * <b> post: </b> El el contenido del nodo se inicializó con los datos dados por parametro, su nodo siguiente es nulo.
		 * @param pData Objeto que se va a almacenar.
		 */
		public Nodo(E pData) {
			data = pData;
			siguiente = null;
		}

		// -----------------------------------------------------------------
		// M�todos
		// -----------------------------------------------------------------

		/**
		 * Retorna el objeto almacenado.
		 * @return Objeto almacenado.
		 */
		public E darData() {
			return data;
		}

		/**
		 * Retorna el siguiente de la cadena.
		 * @return Nodo siguiente de la cadena.
		 */
		public Nodo<E> darSiguiente() {
			return siguiente;
		}

		/**
		 * Asigna al nodo siguiente el dado por parametro
		 */
		public void asignarSiguiente(Nodo<E> pSiguiente) {
			siguiente = pSiguiente;
		}
	}

	/**
	 * Clase que representa el iterador para moverse a trav�s de los elementos guardados. <br>
	 */
	private class Iterador implements Iterator<E> {

		// -----------------------------------------------------------------
		// Atributos
		// -----------------------------------------------------------------

		/**
		 * Nodo actual de la iteración.
		 */
		private Nodo<E> actual;

		// -----------------------------------------------------------------
		// Constructor
		// -----------------------------------------------------------------
		/**
		 * Crea un nuevo iterador. <br>
		 * <b> post: </b> El iterador se creó con su primer elemento siendo el principio de la LinkedLista
		 * @param pData principio, el primer nodo de la LinkedLista a iterar.
		 */
		public Iterador(Nodo<E> principio) {
			actual = principio;
		}

		// -----------------------------------------------------------------
		// Métodos
		// -----------------------------------------------------------------
		/**
		 * Retorna si hay un siguiente elemento para recorrer.
		 * @return true si el nodo siguiente no es nulo, false de lo contrario.
		 */
		public boolean hasNext() {
			boolean rta = true;
			if (actual == null) {
				rta = false;
			}
			return rta;
		}

		/**
		 * Retorna el nodo siguiente de la iteración.
		 * @return Nodo siguiente de la iteración.
		 */
		public E next() {
			E data = actual.darData();
			actual = actual.darSiguiente();
			return data;
		}

		public void remove() {
			// TODO Auto-generated method stub
			
		}

	}

	// -----------------------------------------------------------------
	// Atributos
	// -----------------------------------------------------------------

	/**
	 * Nodo inicio de la LinkedLista
	 */
	private Nodo<E> primero;

	/**
	 * Nodo final de la LinkedLista
	 */
	private Nodo<E> ultimo;

	/**
	 * Tama�o actual de la LinkedLista
	 */
	private int tam;

	// -----------------------------------------------------------------
	// Constructor
	// -----------------------------------------------------------------
	/**
	 * Crea una nueva LinkedLista gen�rica. <br>
	 * <b> post: </b> Se cre� una LinkedLista encadenada gen�rica vac�a.
	 */
	public LinkedList() {
		primero = null;
		ultimo = null;
		tam = 0;
	}
	public LinkedList(E data){
		primero = null;
		ultimo = null;
		tam=0;
		addFirst(data);
	}

	// -----------------------------------------------------------------
	// M�todos
	// -----------------------------------------------------------------

	/**
	 * Agrega un objeto al principio de la LinkedLista.
	 */
	public void addFirst(E data) {
		Nodo<E> n = new Nodo<E>(data);
		if (tam == 0) {
			primero = n;
			ultimo = n;
		} else {
			n.asignarSiguiente(primero);
			primero = n;
		}
		tam++;
	}

	/**
	 * Agrega un objeto al final de la LinkedLista.
	 */
	public void add(E data) {
		Nodo<E> n = new Nodo<E>(data);
		if (tam == 0) {
			primero = n;
			ultimo = n;
		} else {
			ultimo.asignarSiguiente(n);
			ultimo = ultimo.darSiguiente();
		}
		tam++;
	}

	/**
	 * Retorna el primer nodo de la LinkedLista.
	 * @return primero, primer nodo de la LinkedLista
	 */
	public E getFirst() {
		if(primero!=null){
			return primero.darData();
		}
		return null;
	}

	/**
	 * Retorna el �ltimo nodo de la LinkedLista.
	 * @return �ltimo, �ltimo nodo de la LinkedLista
	 */
	public E getLast() {
		return ultimo.darData();
	}

	public E getElem(E elem){
		E rta = null;
		if(elem.equals(primero.darData())){
			rta=primero.darData();
		}
		else{
			Nodo<E> actual = primero;
			boolean encontro = false;
			while(!encontro && actual!=null && actual.darSiguiente() !=null){
				if(actual.darData().equals(elem)){
					rta = actual.data;
					encontro=true;
				}
				else{
					actual = actual.darSiguiente();
				}
			}
		}
		return rta;
	}
	
	public E getRandom()
	{
		int pos = (int) (Math.random()*tam);
		int contador = 0;
		Iterator<E> iter = this.iterator();
		while(iter.hasNext())
		{
			if(contador == pos)
			{
				return iter.next();
			}
			iter.next();
			contador ++;
		}
		
		// Nunca pasa.
		return null;
	}
	
	/**
	 * Elimina y retorna el primer elemento de la LinkedLista.
	 * @return primer elemento de la LinkedLista.
	 */
	public E removeFirst() {
		E data = primero.darData();
		primero = primero.darSiguiente();
		tam--;
		return data;
	}

	/**
	 * Devuelve el tama�o actual de la LinkedLista.
	 * @return tama�o actual.
	 */
	public int size() {
		return tam;
	}

	/**
	 * Devuelve si la LinkedLista se encuentra actualmente vac�a.
	 * @return true si la LinkedLista se encuentra vac�a, false de lo contrario.
	 * Devuelve si la LinkedLista se encuentra actualmente vacía.
	 * @return true si la LinkedLista se encuentra vacía, false de lo contrario.
	 */
	public boolean isEmpty() {
		return (tam == 0);
	}

	/**
	 * Devuelve un iterador para la LinkedLista.
	 * @return iterator.
	 */
	public Iterator<E> iterator() {
		return new Iterador(primero);
	}

	/**
	 * M�todo que ordena la LinkedLista utilizando MergeSort.
	 */
	public void sort(Comparator<E> c) {
		LinkedList<E> l = sort(c, this);
		ultimo = l.ultimo;
		primero = l.primero;
	}

	/**
	 * M�todo recursivo para hacer el sort de Mergesort.
	 */
	public LinkedList<E> sort(Comparator<E> c, LinkedList<E> l) {
		if (l.size() <= 1) {
			return l;
		} else {
			LinkedList<E> l1 = new LinkedList<E>();
			LinkedList<E> l2 = new LinkedList<E>();
			Iterator<E> i_s = l.iterator();
			Iterator<E> i_f = l.iterator();
			int n = 0;
			while (i_f.hasNext()) {
				i_f.next();
				if (n % 2 == 0) {
					l1.add(i_s.next());
				}
				n++;
			}
			while (i_s.hasNext()) {
				l2.add(i_s.next());
			}
			l1 = sort(c, l1);
			l2 = sort(c, l2);
			l = merge(l1, l2, c);
			return l;
		}
	}

	/**
	 * M�todo que hace el merge de MergeSort.
	 */
	private LinkedList<E> merge(LinkedList<E> l1, LinkedList<E> l2, Comparator<E> c) {
		LinkedList<E> rta = new LinkedList<E>();
		while (!l1.isEmpty() && !l2.isEmpty()) {
			int n = c.compare(l1.getFirst(), l2.getFirst());
			if (n <= 0) {
				rta.add(l1.removeFirst());
			} else {
				rta.add(l2.removeFirst());
			}
		}
		while (!l1.isEmpty()) {
			rta.add(l1.removeFirst());
		}
		while (!l2.isEmpty()) {
			rta.add(l2.removeFirst());
		}
		return rta;
	}

	public boolean contains(E t){
		boolean rta=false;
		Nodo<E> actual = primero;
		while(!rta &&  actual!=null){
			if(t.equals(actual.darData())){
				rta=true;
			}
			actual=actual.darSiguiente();
		}
		return rta;
	}

	public E remove(E elemento){
		E rta = null;
		if(elemento.equals(primero.darData())){
			rta=primero.darData();
			primero = primero.darSiguiente();
			tam--;
		}
		else{
			Nodo<E> actual = primero;
			boolean encontro = false;
			while(!encontro && actual!=null && actual.darSiguiente() !=null){
				if(actual.darSiguiente().darData().equals(elemento)){
					encontro=true;
					tam--;
				}
				else{
					actual = actual.darSiguiente();
				}
			}
			if(encontro){
				Nodo<E> temp =actual.darSiguiente();
				rta=temp.darData();
				actual.asignarSiguiente(temp.darSiguiente());
			}
		}

		return rta;
	}
	public boolean equals(LinkedList<E> LinkedLista){
		if(LinkedLista==null){
			return false;
		}
		boolean rta = tam==LinkedLista.tam;
		if(rta){
			Iterator<E> i1 = this.iterator();
			Iterator<E> i2 = LinkedLista.iterator();
			while(i1.hasNext() && rta){
				E e1 = i1.next();
				E e2 = i2.next();
				rta = e1.equals(e2);
			}
		}
		return rta;
	}
	protected void agregarOrdenado(E elemento, Comparator<E> c){

		if(primero==null || c.compare(elemento, primero.darData())>=0){
			addFirst(elemento);
		}
		else{
			Nodo<E> n = new Nodo<E>(elemento);
			Nodo<E> actual = primero;
			boolean agrego = false;
			while(!agrego){
				if(actual.darSiguiente()==null){
					actual.siguiente=n;
					agrego=true;
					tam++;
				}
				else if(c.compare(elemento, actual.darSiguiente().darData())>=0){
					n.siguiente=actual.siguiente;
					actual.siguiente=n;
					agrego=true;
					tam++;
				}
				actual=actual.siguiente;
			}
		}


	}
}