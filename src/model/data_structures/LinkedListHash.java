package model.data_structures;

import java.util.Comparator;
import java.util.Iterator;


/**
 * Clase que representa la LinkedLista encadenada gen�rica a utilizar. <br>
 * Modificado el nombre de la estructura de LinkedLista a LinkedList.
 */
public class LinkedListHash<K,V> implements Iterable<K> {

	// -----------------------------------------------------------------
	// Atributos
	// -----------------------------------------------------------------

	/**
	 * Clase que representa a un nodo gen�rico. <br>
	 */
	@SuppressWarnings("hiding")
	public static class Nodo<K,V> {

		// -----------------------------------------------------------------
		// Atributos
		// -----------------------------------------------------------------

		/**
		 * Objeto que representa la llave
		 */
		private K key;

		
		/**
		 * Objeto que se va a guardar
		 */
		private V value;

		/**
		 * Siguiente nodo en la LinkedLista encadenada
		 */
		private Nodo<K,V> siguiente;


		// -----------------------------------------------------------------
		// Constructor
		// -----------------------------------------------------------------

		/**
		 * Crea un nuevo nodo gen�rico. <br>
		 * <b> post: </b> El el contenido del nodo se inicializ� con los datos dados por parametro, su nodo siguiente es nulo.
		 * Crea un nuevo nodo genérico. <br>
		 * <b> post: </b> El el contenido del nodo se inicializó con los datos dados por parametro, su nodo siguiente es nulo.
		 * @param pvalue Objeto que se va a almacenar.
		 */
		public Nodo(V pvalue, K pkey) {
			key = pkey;
			value = pvalue;
			siguiente = null;
		}

		// -----------------------------------------------------------------
		// M�todos
		// -----------------------------------------------------------------

		/**
		 * Retorna el objeto almacenado.
		 * @return Objeto almacenado.
		 */
		public V darvalue() {
			return value;
		}

		public K darLlave(){
			return key;
		}
		
		public void setLlave(K key){
			this.key = key;
		}
		
		
		public void setValue(V Value){
			this.value = value;
		}
		
		/**
		 * Retorna el siguiente de la cadena.
		 * @return Nodo siguiente de la cadena.
		 */
		public Nodo<K,V> darSiguiente() {
			return siguiente;
		}

		/**
		 * Asigna al nodo siguiente el dado por parametro
		 */
		public void asignarSiguiente(Nodo<K,V> pSiguiente) {
			siguiente = pSiguiente;
		}
	}

	/**
	 * Clase que representa el iterador para moverse a trav�s de los elementos guardados. <br>
	 */
	private class Iterador implements Iterator<K> {

		// -----------------------------------------------------------------
		// Atributos
		// -----------------------------------------------------------------

		/**
		 * Nodo actual de la iteración.
		 */
		private Nodo<K,V> actual;

		// -----------------------------------------------------------------
		// Constructor
		// -----------------------------------------------------------------
		/**
		 * Crea un nuevo iterador. <br>
		 * <b> post: </b> El iterador se creó con su primer elemento siendo el principio de la LinkedLista
		 * @param pvalue principio, el primer nodo de la LinkedLista a iterar.
		 */
		public Iterador(Nodo<K,V> principio) {
			actual = principio;
		}

		// -----------------------------------------------------------------
		// Métodos
		// -----------------------------------------------------------------
		/**
		 * Retorna si hay un siguiente elemento para recorrer.
		 * @return true si el nodo siguiente no es nulo, false de lo contrario.
		 */
		public boolean hasNext() {
			boolean rta = true;
			if (actual == null) {
				rta = false;
			}
			return rta;
		}

		public Nodo<K,V> darActual(){
			return actual;
		}
		
		/**
		 * Retorna el nodo siguiente de la iteración.
		 * @return Nodo siguiente de la iteración.
		 */
		public K next() {
			K value = actual.darLlave();
			actual = actual.darSiguiente();
			return value;
		}

		public void remove() {
			// TODO Auto-generated method stub
			
		}

	}

	// -----------------------------------------------------------------
	// Atributos
	// -----------------------------------------------------------------

	/**
	 * Nodo inicio de la LinkedLista
	 */
	private Nodo<K,V> primero;

	/**
	 * Nodo final de la LinkedLista
	 */
	private Nodo<K,V> ultimo;

	/**
	 * Tama�o actual de la LinkedLista
	 */
	private int tam;

	// -----------------------------------------------------------------
	// Constructor
	// -----------------------------------------------------------------
	/**
	 * Crea una nueva LinkedLista gen�rica. <br>
	 * <b> post: </b> Se cre� una LinkedLista encadenada gen�rica vac�a.
	 */
	public LinkedListHash() {
		primero = null;
		ultimo = null;
		tam = 0;
	}
	public LinkedListHash(V value, K key){
		primero = null;
		ultimo = null;
		tam=0;
		addFirst(value, key);
	}

	// -----------------------------------------------------------------
	// M�todos
	// -----------------------------------------------------------------

	/**
	 * Agrega un objeto al principio de la LinkedLista.
	 */
	public void addFirst(V value, K key) {
		Nodo<K,V> n = new Nodo<K,V>(value,key);
		if (tam == 0) {
			primero = n;
			ultimo = n;
		} else {
			n.asignarSiguiente(primero);
			primero = n;
		}
		tam++;
	}

	/**
	 * Agrega un objeto al final de la LinkedLista.
	 */
	public void add(V value, K key) {
		Nodo<K,V> n = new Nodo<K,V>(value,key);
		if (tam == 0) {
			primero = n;
			ultimo = n;
		} else {
			ultimo.asignarSiguiente(n);
			ultimo = ultimo.darSiguiente();
		}
		tam++;
	}

	/**
	 * Retorna el primer nodo de la LinkedLista.
	 * @return primero, primer nodo de la LinkedLista
	 */
	public Nodo<K,V> getFirst() {
		if(primero!=null){
			return primero;
		}
		return null;
	}

	/**
	 * Retorna el �ltimo nodo de la LinkedLista.
	 * @return �ltimo, �ltimo nodo de la LinkedLista
	 */
	public Nodo<K,V> getLast() {
		return ultimo;
	}
	
	public V getElem(K elem){
		V rta = null;
		if(elem.equals(primero.darLlave())){
			rta=primero.darvalue();
		}
		else{
			Nodo<K,V> actual = primero;
			boolean encontro = false;
			while(!encontro && actual!=null && actual.darSiguiente() !=null){
				if(actual.darLlave().equals(elem)){
					rta = actual.value;
					encontro=true;
				}
				else{
					actual = actual.darSiguiente();
				}
			}
		}
		return rta;
	}

	/**
	 * Elimina y retorna el primer elemento de la LinkedLista.
	 * @return primer elemento de la LinkedLista.
	 */
	public Nodo<K,V> removeFirst() {
		Nodo<K,V> value = primero;
		primero = primero.darSiguiente();
		tam--;
		return value;
	}

	/**
	 * Devuelve el tama�o actual de la LinkedLista.
	 * @return tama�o actual.
	 */
	public int size() {
		return tam;
	}

	/**
	 * Devuelve si la LinkedLista se encuentra actualmente vac�a.
	 * @return true si la LinkedLista se encuentra vac�a, false de lo contrario.
	 * Devuelve si la LinkedLista se encuentra actualmente vacía.
	 * @return true si la LinkedLista se encuentra vacía, false de lo contrario.
	 */
	public boolean isEmpty() {
		return (tam == 0);
	}

	/**
	 * Devuelve un iterador para la LinkedLista.
	 * @return iterator.
	 */
	public Iterator<K> iterator() {
		return new Iterador(primero);
	}

	/**
	 * M�todo que ordena la LinkedLista utilizando MergeSort.
	 */
	public void sort(Comparator<V> c) {
		LinkedListHash<K,V> l = sort(c, this);
		ultimo = l.ultimo;
		primero = l.primero;
	}

	/**
	 * M�todo recursivo para hacer el sort de Mergesort.
	 */
	public LinkedListHash<K,V> sort(Comparator<V> c, LinkedListHash<K, V> l) {
		if (l.size() <= 1) {
			return l;
		} else {
			LinkedListHash<K,V> l1 = new LinkedListHash<K,V>();
			LinkedListHash<K,V> l2 = new LinkedListHash<K,V>();
			Iterador i_s = (LinkedListHash<K, V>.Iterador) l.iterator();
			Iterador i_f = (LinkedListHash<K, V>.Iterador) l.iterator();
			int n = 0;
			while (i_f.hasNext()) {
				i_f.next();
				if (n % 2 == 0) {
					Nodo <K,V> aux = i_s.actual;
					l1.add(aux.value,aux.key);
				}
				n++;
			}
			while (i_s.hasNext()) {
				Nodo <K,V> aux = i_s.actual;
				l2.add(aux.value,aux.key);
			}
			l1 = sort(c, l1);
			l2 = sort(c, l2);
			l = merge(l1, l2, c);
			return l;
		}
	}

	/**
	 * M�todo que hace el merge de MergeSort.
	 */
	private LinkedListHash<K,V> merge(LinkedListHash<K,V> l1, LinkedListHash<K,V> l2, Comparator<V> c) {
		LinkedListHash<K,V> rta = new LinkedListHash<K,V>();
		while (!l1.isEmpty() && !l2.isEmpty()) {
			int n = c.compare(l1.getFirst().value, l2.getFirst().value);
			if (n <= 0) {
				Nodo<K,V> aux = l1.removeFirst();
				rta.add(aux.value,aux.key);
			} else {
				Nodo<K,V> aux = l2.removeFirst();
				rta.add(aux.value,aux.key);
			}
		}
		while (!l1.isEmpty()) {
			Nodo<K,V> aux = l1.removeFirst();
			rta.add(aux.value,aux.key);
		}
		while (!l2.isEmpty()) {
			Nodo<K,V> aux = l2.removeFirst();
			rta.add(aux.value,aux.key);
		}
		return rta;
	}

	public boolean contains(K t){
		boolean rta=false;
		Nodo<K,V> actual = primero;
		while(!rta &&  actual!=null){
			if(t.equals(actual.darLlave())){
				rta=true;
			}
			actual=actual.darSiguiente();
		}
		return rta;
	}

	public Nodo<K,V> remove(K key){
		
		Nodo<K,V> rta = null;
		if(key.equals(primero.darLlave())){
			rta= primero;
			primero = primero.darSiguiente();
			tam--;
		}
		else{
			Nodo<K,V> actual = primero;
			boolean encontro = false;
			while(!encontro && actual!=null && actual.darSiguiente() !=null){
				if(actual.darSiguiente().darLlave().equals(key)){
					encontro=true;
					tam--;
				}
				else{
					actual = actual.darSiguiente();
				}
			}
			if(encontro){
				Nodo<K,V> temp =actual.darSiguiente();
				rta=temp;
				actual.asignarSiguiente(temp.darSiguiente());
			}
		}

		return rta;
	}
	public boolean equals(LinkedListHash<K,V> LinkedLista){
		if(LinkedLista==null){
			return false;
		}
		boolean rta = tam==LinkedLista.tam;
		if(rta){
			Iterator<K> i1 = this.iterator();
			Iterator<K> i2 = LinkedLista.iterator();
			while(i1.hasNext() && rta){
				K e1 = i1.next();
				K e2 = i2.next();
				rta = e1.equals(e2);
			}
		}
		return rta;
	}
	protected void agregarOrdenado(V value, K key, Comparator<V> c){

		if(primero==null || c.compare(value, primero.darvalue())>=0){
			addFirst(value,key);
		}
		else{
			Nodo<K,V> n = new Nodo<K,V>(value,key);
			Nodo<K,V> actual = primero;
			boolean agrego = false;
			while(!agrego){
				if(actual.darSiguiente()==null){
					actual.siguiente=n;
					agrego=true;
					tam++;
				}
				else if(c.compare(value, actual.darSiguiente().darvalue())>=0){
					n.siguiente=actual.siguiente;
					actual.siguiente=n;
					agrego=true;
					tam++;
				}
				actual=actual.siguiente;
			}
		}


	}
}