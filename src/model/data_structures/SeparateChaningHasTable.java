package model.data_structures;

import java.util.Comparator;
import java.util.Iterator;
import model.data_structures.LinkedListHash.Nodo;

public class SeparateChaningHasTable<K,V> implements IHashTable<K, V>{

	private final static double FACTOR = 0.6;
	private int size = 0;
	private int capacity;
	private LinkedListHash<K, V>[] array;

	public SeparateChaningHasTable(int capacity) {
		this.capacity = capacity;
		array = new LinkedListHash[capacity];
		for (int i = 0; i < capacity; i++) {
			array[i] = new LinkedListHash<K,V>();
		}
	}
	
	public SeparateChaningHasTable() {
		this(1);
	}

	//Hashea la llave para ubicarla dentro del arreglo
	public int hashIndex(K key) {
		int hashKey = key.hashCode();
		int index = hashKey%capacity;
		return index;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public LinkedListHash<K, V>[] getArray() {
		return array;
	}

	public void setArray(LinkedListHash<K, V>[] array) {
		this.array = array;
	}

	private int p = 0;
	//Ubica el nodo en la ultima posicion de la lista segun su llave
	@Override
	public void put(K key, V value) {
		if(key==null) return;
		int index = hashIndex(key);
		if(index<1) index*=-1;
		LinkedListHash<K, V> indexList = array[index];
		boolean exist = false;
		if(indexList.getFirst()!=null){
			Nodo<K,V> nodo = indexList.getFirst();
			while(nodo!=null&&!exist)
				{
					if(nodo.darLlave().equals(key))
					{
						nodo.setValue(value);
						return;
					}
					nodo=nodo.darSiguiente();
				}
		}
		indexList.add(value, key);
		size++;
		if((double)size/capacity>=FACTOR) reHash();
	}
	//Se obtiene el valor del nodo segun su llave
	@Override
	public V get(K key) {
		if(key==null) return null;
		else{
			int index = hashIndex(key);
			LinkedListHash<K, V> indexList = array[index];
			return indexList.getElem(key);
		}
	}


	//Se borra un nodo de la lista segun su llave
	@Override
	public V delete(K key) {
		if(key==null)return null;
		int index = hashIndex(key);
		LinkedListHash<K, V> indexList = array[index];
		size--;
		V rta = indexList.getElem(key); 
		indexList.remove(key);
		return rta;
	}

	@Override
	public Iterator<K> keys() {
		// TODO Auto-generated method stub
		return keysArray().iterator();
	}

	public Iterator<K> iterador(){
		return keys();
	}

	public ArrayList<K> keysArray(){
		ArrayList<K> keysArray = new ArrayList<K>();
		for (int i = 0; i < array.length; i++) {
			LinkedListHash<K, V> aux = array[i];
			for (K key : aux) {
				keysArray.add(key);
			}
		}
		return keysArray;
	}

	private void reHash() {
		LinkedListHash<K, V>[] temp = array;
		capacity = 2 * capacity;
		array = new LinkedListHash[capacity];
		for(int i=0;i<temp.length;i++)
		{
			array[i] = temp[i];
		}
		for (int i = temp.length; i < capacity; i++)
		{
			array[i] = (new LinkedListHash<K,V>());
		}
	}
	
	public ArrayList<V> getAll(K key){
		ArrayList<V> rta = new ArrayList<V>();
		int index = hashIndex(key);
		LinkedListHash<K, V> indexList = array[index];
		for (K keyList : indexList) {
			rta.add(indexList.getElem(keyList));
		}
		return rta;
		
		
	}
}
