package model.data_structures;

import java.util.Comparator;
import java.util.Iterator;


// TODO: Auto-generated Javadoc
/**
 * Clase que representa la LinkedLista encadenada gen�rica a utilizar. <br>
<<<<<<< HEAD
 * @author https://bitbucket.org/korkies22/
 * Modificado el nombre de la estructura de Lista a LinkedList.
=======
 *
 * Modificado el nombre de la estructura de LinkedLista a LinkedList.
>>>>>>> 59471e478eaa2a4888c79d16ce7b8b733f01d809
 * @param <E> the element type
 */
public class VOLinkedList<E extends Comparable<E>> implements Iterable<E> {

	// ------------------------------------------------------------------------------------
	// ------------------------- Atributos ------------------------------------------------
	// ------------------------------------------------------------------------------------

	/** Nodo inicio de la LinkedLista. */
	private Node<E> primero;

	/** Nodo final de la LinkedLista. */
	private Node<E> ultimo;

	/** Tama�o actual de la LinkedLista. */
	private int tam;

	// ------------------------------------------------------------------------------------
	// ------------------------- M�todo Constructor ---------------------------------------
	// ------------------------------------------------------------------------------------

	/**
	 * Crea una nueva LinkedLista gen�rica. <br>
	 * <b> post: </b> Se cre� una LinkedLista encadenada gen�rica vac�a.
	 */
	public VOLinkedList() {
		primero = null;
		ultimo = null;
		tam = 0;
	}

	/**
	 * Instantiates a new linked list.
	 *
	 * @param data the data
	 */
	public VOLinkedList(E data){
		primero = null;
		ultimo = null;
		tam=0;
		addFirst(data);
	}

	// ------------------------------------------------------------------------------------
	// ------------------------- M�todos --------------------------------------------------
	// ------------------------------------------------------------------------------------

	/**
	 * Agrega un objeto al principio de la LinkedLista.
	 *
	 * @param data the data
	 */
	public void addFirst(E data) {
		Node<E> n = new Node<E>(data);
		if (tam == 0) {
			primero = n;
			ultimo = n;
		} else {
			n.setNext(primero);
			primero = n;
		}
		tam++;
	}

	/**
	 * Agrega un objeto al final de la LinkedLista.
	 *
	 * @param data the data
	 */
	public void add(E data) {
		Node<E> n = new Node<E>(data);
		if (tam == 0) {
			primero = n;
			ultimo = n;
		} else {
			ultimo.setNext(n);
			ultimo = ultimo.getNext();
		}
		tam++;
	}

	/**
	 * Retorna el primer nodo de la LinkedLista.
	 * @return primero, primer nodo de la LinkedLista
	 */
	public E getFirst() {
		if(primero!=null){
			return primero.getValue();
		}
		return null;
	}

	/**
	 * Retorna el �ltimo nodo de la LinkedLista.
	 * @return �ltimo, �ltimo nodo de la LinkedLista
	 */
	public E getLast() {
		return ultimo.getValue();
	}

	/**
	 * Removes the elemento.
	 * @param elemento the elemento
	 * @return the e
	 */
	public E remove(E elemento){
		E rta = null;
		if(elemento.equals(primero.getValue())){
			rta=primero.getValue();
			primero = primero.getNext();
			tam--;
		}
		else{
			Node<E> actual = primero;
			boolean encontro = false;
			while(!encontro && actual!=null && actual.getNext() !=null){
				if(actual.getNext().getValue().equals(elemento)){
					encontro=true;
					tam--;
				}
				else{
					actual = actual.getNext();
				}
			}
			if(encontro){
				Node<E> temp =actual.getNext();
				rta=temp.getValue();
				actual.setNext(temp.getNext());
			}
		}

		return rta;
	}

	/**
	 * Elimina y retorna el primer elemento de la LinkedLista.
	 * @return primer elemento de la LinkedLista.
	 */
	public E removeFirst() {
		E data = primero.getValue();
		primero = primero.getNext();
		tam--;
		return data;
	}

	/**
	 * Devuelve el tama�o actual de la LinkedLista.
	 * @return tama�o actual.
	 */
	public int size() {
		return tam;
	}

	/**
	 * Devuelve si la LinkedLista se encuentra actualmente vac�a.
	 * @return true si la LinkedLista se encuentra vac�a, false de lo contrario.
	 * Devuelve si la LinkedLista se encuentra actualmente vacía.
	 * true si la LinkedLista se encuentra vacía, false de lo contrario.
	 */
	public boolean isEmpty() {
		return (tam == 0);
	}

	/**
	 * Devuelve un iterador para la LinkedLista.
	 * @return iterator.
	 */
	public Iterator<E> iterator() {
		return new Iterador<E>(primero);
	}

	/**
	 * M�todo que ordena la LinkedLista utilizando MergeSort.
	 * @param c the c
	 */
	public void sort(Comparator<E> c) {
		VOLinkedList<E> l = sort(c, this);
		ultimo = l.ultimo;
		primero = l.primero;
	}

	/**
	 * M�todo recursivo para hacer el sort de Mergesort.
	 * @param c the c
	 * @param l the l
	 * @return the linked list
	 */
	public VOLinkedList<E> sort(Comparator<E> c, VOLinkedList<E> l) {
		if (l.size() <= 1) {
			return l;
		} else {
			VOLinkedList<E> l1 = new VOLinkedList<E>();
			VOLinkedList<E> l2 = new VOLinkedList<E>();
			Iterator<E> i_s = l.iterator();
			Iterator<E> i_f = l.iterator();
			int n = 0;
			while (i_f.hasNext()) {
				i_f.next();
				if (n % 2 == 0) {
					l1.add(i_s.next());
				}
				n++;
			}
			while (i_s.hasNext()) {
				l2.add(i_s.next());
			}
			l1 = sort(c, l1);
			l2 = sort(c, l2);
			l = merge(l1, l2, c);
			return l;
		}
	}

	/**
	 * M�todo que hace el merge de MergeSort.
	 * @param l1 the l 1
	 * @param l2 the l 2
	 * @param c the c
	 * @return the linked list
	 */
	private VOLinkedList<E> merge(VOLinkedList<E> l1, VOLinkedList<E> l2, Comparator<E> c) {
		VOLinkedList<E> rta = new VOLinkedList<E>();
		while (!l1.isEmpty() && !l2.isEmpty()) {
			int n = c.compare(l1.getFirst(), l2.getFirst());
			if (n <= 0) {
				rta.add(l1.removeFirst());
			} else {
				rta.add(l2.removeFirst());
			}
		}
		while (!l1.isEmpty()) {
			rta.add(l1.removeFirst());
		}
		while (!l2.isEmpty()) {
			rta.add(l2.removeFirst());
		}
		return rta;
	}

	/**
	 * Contains.
	 * @param t the t
	 * @return true, if successful
	 */
	public boolean contains(E t){
		boolean rta=false;
		Node<E> actual = primero;
		while(!rta &&  actual!=null){
			if(t.equals(actual)){
				rta=true;
			}
			actual=actual.getNext();
		}
		return rta;
	}

	/**
	 * Equals.
	 * @param LinkedLista the linked lista
	 * @return true, if successful
	 */
	public boolean equals(VOLinkedList<E> LinkedLista){
		if(LinkedLista==null){
			return false;
		}
		boolean rta = tam==LinkedLista.tam;
		if(rta){
			Iterator<E> i1 = this.iterator();
			Iterator<E> i2 = LinkedLista.iterator();
			while(i1.hasNext() && rta){
				E e1 = i1.next();
				E e2 = i2.next();
				rta = e1.equals(e2);
			}
		}
		return rta;
	}

	/**
	 * Adds the sorted.
	 * @param elemento the elemento
	 * @param c the c
	 */
	protected void addSorted(E elemento, Comparator<E> c){

		if(primero==null || c.compare(elemento, primero.getValue())>=0){
			addFirst(elemento);
		}
		else{
			Node<E> n = new Node<E>(elemento);
			Node<E> actual = primero;
			boolean agrego = false;
			while(!agrego){
				if(actual.getNext()==null){
					actual.setNext(n);
					agrego=true;
					tam++;
				}
				else if(c.compare(elemento, actual.getNext().getValue())>=0){
					n.setNext(actual.getNext());
					actual.setNext(n);
					agrego=true;
					tam++;
				}
				actual=actual.getNext();
			}
		}


	}
}