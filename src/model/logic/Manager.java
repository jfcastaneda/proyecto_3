package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.NoSuchElementException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import api.IManager;
import model.data_structures.DiGraph;
import model.data_structures.RedBlackBST;
import model.template.TemplateModifier;
import model.data_structures.DiGraph.Vertex;
import model.data_structures.LinearProbingHashST;
import model.data_structures.LinkedList;
import model.vo.*;

public class Manager implements IManager {

	// ------------------------------------------------------------------------------------
	// ------------------------- Constantes
	// -----------------------------------------------
	// ------------------------------------------------------------------------------------

	/**
	 * Direcci�n del archivo Json peque�o.
	 */
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";

	/**
	 * Direcci�n del archivo Json mediano.
	 */
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";

	/**
	 * Direcci�n del archivo Json grande.
	 */
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";

	public static final String NULL_SERVICE_ID = "No Id for the service";

	private static final long MILISE_TO_SEC = (long) 60 * (long) 15 * (long) 1000;

	private static final double MILE_TO_METER = 1609.34;

	private DiGraph<String, VOPuntoCiudad, VOPeso> grafoPorDistancias;

	private DiGraph<String, VOPuntoCiudad, VOPeso> grafoPorTiempo;

	private static final double NO_NUMERIC_DATA = Double.MAX_VALUE;

	private LinkedList<String> chicagoStreets;

	public Manager() {
	}

	@Override
	public int darNumeroVertices() {
		// TODO Auto-generated method stub
		return grafoPorDistancias.V();
	}

	@Override
	public int darNumeroDeArcos() {
		// TODO Auto-generated method stub
		return grafoPorDistancias.E();
	}

	/**
	 * M�todo que carga la informaci�n del grafo desde el archivo Json.
	 */
	public void cargarJson() {
		grafoPorDistancias = new DiGraph<String, VOPuntoCiudad, VOPeso>();
		grafoPorTiempo = new DiGraph<String, VOPuntoCiudad, VOPeso>(VOPeso.COMPARADOR_TIEMPO);
		JsonParser parser = new JsonParser();
		int i = 0;
		try {
			JsonArray array = (JsonArray) parser.parse(new FileReader("./data/JsonGrafo.json"));
			for (JsonElement jsonElement : array) {
				JsonObject actual = (JsonObject) jsonElement;
				// Llave para vertice
				String llave = actual.get("key").getAsString();
				JsonObject puntoCiudadActual = actual.getAsJsonObject("datosPuntoCiudad");
				double latitud = puntoCiudadActual.get("latitud").getAsDouble();
				double longitud = puntoCiudadActual.get("longitud").getAsDouble();
				JsonArray idServiciosCabeza = puntoCiudadActual.getAsJsonArray("idServiciosCabeza");
				LinkedList<String> idServCa = new LinkedList<String>();
				for (JsonElement idServicios : idServiciosCabeza) {
					idServCa.add(idServicios.getAsString());
				}
				JsonArray idServiciosCola = puntoCiudadActual.getAsJsonArray("idServiciosCola");
				LinkedList<String> idServCo = new LinkedList<String>();
				for (JsonElement idServicios : idServiciosCola) {
					idServCo.add(idServicios.getAsString());
				}
				// Se agrega el vertice
				VOPuntoCiudad puntoCiudad = new VOPuntoCiudad(idServCa, idServCo, latitud, longitud, llave);
				grafoPorDistancias.addVertex(llave, puntoCiudad);
				grafoPorTiempo.addVertex(llave, puntoCiudad);

				// Se comienzan a crear los arcos asociados a ese vertice
				JsonArray arcos = actual.getAsJsonArray("arcos");
				for (JsonElement arco : arcos) {
					i++;
					JsonObject arcoActual = (JsonObject) arco;
					String idComienzo = arcoActual.get("idComienzo").getAsString();
					String idFin = arcoActual.get("idFin").getAsString();
					double numServicios = arcoActual.get("numero_de_servicios").getAsDouble();
					double distanciaRecorria = (arcoActual.get("distancia_recorrida").getAsDouble() != 0)
							? arcoActual.get("distancia_recorrida").getAsDouble()
							: NO_NUMERIC_DATA;
					double valorServicios = (arcoActual.get("valor_servicios").getAsDouble() != 0)
							? arcoActual.get("valor_servicios").getAsDouble()
							: NO_NUMERIC_DATA;
					double valorPeajes = arcoActual.get("valor_peajes").getAsDouble();
					double tiempoServicios = arcoActual.get("tiempo_servicio").getAsDouble();
					VOPeso pesoActual = new VOPeso(numServicios, distanciaRecorria, valorServicios, valorPeajes,
							tiempoServicios, idComienzo, idFin);
					grafoPorDistancias.addEdge(idComienzo, idFin, pesoActual);
					grafoPorTiempo.addEdge(idComienzo, idFin, pesoActual);
				}
			}

			chicagoStreets = new LinkedList<String>();
			String csvFile = "data" + File.separator + "ChicagoStreets.csv";
			BufferedReader br = new BufferedReader(new FileReader(csvFile));
			String line = br.readLine();
			while (line != null) {
				String[] lineSplit = line.split(";");
				for (String string : lineSplit) {
					string = string.trim();
					if (string.length() > 30) {
						chicagoStreets.add(string);
					}
				}
				line = br.readLine();
			}
			br.close();

		} catch (JsonIOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public VOPuntoCiudad darVerticeMasCongestionado() {
		VOPuntoCiudad ans = null;
		Iterable<String> llaves = grafoPorDistancias.keys();
		for (String llave : llaves) {
			VOPuntoCiudad iterable = grafoPorDistancias.getInfoVertex(llave);
			if (ans == null)
				ans = iterable;
			else if (iterable.getIdServiciosCabeza().size()
					+ iterable.getIdServiciosCola().size() > ans.getIdServiciosCabeza().size()
							+ ans.getIdServiciosCola().size())
				ans = iterable;
		}
		try {
			TemplateModifier.dibujoRequerimiento1(ans.getLongitud(), ans.getLatitud());
			File f = new File("template" + File.separator + "mapaR1.html");
			java.awt.Desktop.getDesktop().browse(f.toURI());
		} catch (IOException e) {
			e.printStackTrace();
		}

		return ans;
	}

	// Integrar lo que esta en este metodo al que es, a las 5 a.m. no pienso bien :v
	public void caminosNoPeaje() {
		String random1 = chicagoStreets.getRandom();
		System.out.println("El primer punto es: " + random1);
		double lat1 = Double.parseDouble(random1.split(" ")[1]);
		double lon1 = Double.parseDouble(random1.split(" ")[0]);
		String random2 = chicagoStreets.getRandom();
		System.out.println("El segundo punto es: " + random2);
		double lat2 = Double.parseDouble(random2.split(" ")[1]);
		double lon2 = Double.parseDouble(random2.split(" ")[0]);
		TemplateModifier.dibujoRequerimiento6Verdadero(lat1, lon1, lat2, lon2);
		try {
			File f = new File("template" + File.separator + "mapaR6Verdadero.html");
			java.awt.Desktop.getDesktop().browse(f.toURI());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String darCaminoDeMenorDistancia() {
		String random1 = chicagoStreets.getRandom();
		System.out.println("El primer punto es: " + random1);
		double lat1 = Double.parseDouble(random1.split(" ")[1]);
		double lon1 = Double.parseDouble(random1.split(" ")[0]);
		String random2 = chicagoStreets.getRandom();
		System.out.println("El segundo punto es: " + random2);
		double lat2 = Double.parseDouble(random2.split(" ")[1]);
		double lon2 = Double.parseDouble(random2.split(" ")[0]);
		TemplateModifier.dibujoRequerimiento6(lat1, lon1, lat2, lon2);
		try {
			File f = new File("template" + File.separator + "mapaR6.html");
			java.awt.Desktop.getDesktop().browse(f.toURI());
		} catch (IOException e) {
			e.printStackTrace();
		}
		VOPuntoCiudad puntoSalida = puntoMasCercano(lat1, lon1);
		VOPuntoCiudad puntoLlegada = puntoMasCercano(lat2, lon2);
		System.out.println(grafoPorDistancias.Dijkstra(puntoSalida.getIdVertice(), puntoLlegada.getIdVertice()));
		return grafoPorDistancias.Dijkstra(puntoSalida.getIdVertice(), puntoLlegada.getIdVertice());
	}

	public String[] darCaminosMasCortoEnTiempo() {
		String random1 = chicagoStreets.getRandom();
		System.out.println("El primer punto es: " + random1);
		double lat1 = Double.parseDouble(random1.split(" ")[1]);
		double lon1 = Double.parseDouble(random1.split(" ")[0]);
		String random2 = chicagoStreets.getRandom();
		System.out.println("El segundo punto es: " + random2);
		double lat2 = Double.parseDouble(random2.split(" ")[1]);
		double lon2 = Double.parseDouble(random2.split(" ")[0]);
		TemplateModifier.dibujoRequerimiento6(lat1, lon1, lat2, lon2);
		try {
			File f = new File("template" + File.separator + "mapaR6.html");
			java.awt.Desktop.getDesktop().browse(f.toURI());
		} catch (IOException e) {
			e.printStackTrace();
		}
		VOPuntoCiudad puntoSalida = puntoMasCercano(lat1, lon1);
		VOPuntoCiudad puntoLlegada = puntoMasCercano(lat2, lon2);
		String[] ans = new String[2];
		String caminoIda = grafoPorTiempo.Dijkstra(puntoSalida.getIdVertice(), puntoLlegada.getIdVertice());
		String caminoVuelta = grafoPorTiempo.Dijkstra(puntoLlegada.getIdVertice(), puntoSalida.getIdVertice());
		ans[0] = caminoIda;
		ans[1] = caminoVuelta;
		return ans;
	}

	public LinkedList<VOPeso> darInformacionArcosCaminoMenorDistancia(double lat1, double lon1, double lat2,
			double lon2) {
		VOPuntoCiudad puntoSalida = puntoMasCercano(lat1, lon1);
		VOPuntoCiudad puntoLlegada = puntoMasCercano(lat2, lon2);
		LinkedList<VOPeso> listaPesos = new LinkedList<VOPeso>();
		LinkedList<VOPeso> respuesta = grafoPorDistancias.darArcosDijkstra(puntoSalida.getIdVertice(),
				puntoLlegada.getIdVertice(), listaPesos);

		return respuesta;
	}

	public LinkedList<VOPeso> darInformacionArcosCaminoIdaTiempo() {
		System.out.println("Se muestra el mapa del camino mas corto");
		String random1 = chicagoStreets.getRandom();
		System.out.println(random1);
		double lat1 = Double.parseDouble(random1.split(" ")[1]);
		double lon1 = Double.parseDouble(random1.split(" ")[0]);
		String random2 = chicagoStreets.getRandom();
		System.out.println(random2);
		double lat2 = Double.parseDouble(random2.split(" ")[1]);
		double lon2 = Double.parseDouble(random2.split(" ")[0]);
		TemplateModifier.dibujoRequerimiento6(lat1, lon1, lat2, lon2);
		try {
			File f = new File("template" + File.separator + "mapaR6.html");
			java.awt.Desktop.getDesktop().browse(f.toURI());
		} catch (IOException e) {
			e.printStackTrace();
		}
		// De aqu� para arriba es lo que se debe desplazar si esto est� colocado donde
		// no es.
		VOPuntoCiudad puntoSalida = puntoMasCercano(lat1, lon1);
		VOPuntoCiudad puntoLlegada = puntoMasCercano(lat2, lon2);
		LinkedList<VOPeso> listaPesos = new LinkedList<VOPeso>();
		return grafoPorTiempo.darArcosDijkstra(puntoSalida.getIdVertice(), puntoLlegada.getIdVertice(), listaPesos);
	}

	public LinkedList<VOPeso> darInformacionArcosCaminoVueltaTiempo(double lat1, double lon1, double lat2,
			double lon2) {
		VOPuntoCiudad puntoSalida = puntoMasCercano(lat1, lon1);
		VOPuntoCiudad puntoLlegada = puntoMasCercano(lat2, lon2);
		LinkedList<VOPeso> listaPesos = new LinkedList<VOPeso>();
		return grafoPorTiempo.darArcosDijkstra(puntoLlegada.getIdVertice(), puntoSalida.getIdVertice(), listaPesos);
	}

	public LinkedList<VOPeso> todosLosCaminosPosibles(String idComienzo, String idFin) {
		LinkedList<VOPeso> ans = new LinkedList<VOPeso>();
		if (grafoPorTiempo.hayCamino(idComienzo, idFin)) {
			todosLosCaminosPosibles(idComienzo, idComienzo, ans);
			return ans;
		} else {
			return null;
		}
	}

	private void todosLosCaminosPosibles(String idComienzo, String idFin, LinkedList<VOPeso> lista) {
		VOPuntoCiudad origenActual = grafoPorTiempo.getInfoVertex(idComienzo);
		origenActual.setVisitado(true);
		if (idComienzo.equals(idFin)) {
			lista.add(grafoPorTiempo.getInfoArc(idComienzo, idFin));
		} else {
			Iterable<String> IdAdj = grafoPorTiempo.adj(idComienzo);
			for (String idAdjacente : IdAdj) {
				VOPuntoCiudad puntoAdjacente = grafoPorTiempo.getInfoVertex(idAdjacente);
				if (!puntoAdjacente.isVisitado() && grafoPorTiempo.hayCamino(idAdjacente, idFin)) {
					if (grafoPorTiempo.getInfoArc(idAdjacente, idFin).getValorPeaje() == 0) {
						lista.add(grafoPorTiempo.getInfoArc(idComienzo, idAdjacente));
						todosLosCaminosPosibles(idAdjacente, idFin, lista);
						lista.remove(grafoPorTiempo.getInfoArc(idComienzo, idAdjacente));
					}
				}
			}
			origenActual.setVisitado(false);
		}
	}

	private void setVisitadoFalse() {
		Iterable<String> llaves = grafoPorDistancias.keys();
		for (String llave : llaves) {
			grafoPorDistancias.getInfoVertex(llave).setVisitado(false);
			grafoPorTiempo.getInfoVertex(llave).setVisitado(false);
		}
	}

	private VOPuntoCiudad puntoMasCercano(double latitud, double longitud) {
		RedBlackBST<Double, VOPuntoCiudad> puntoMasCercano = new RedBlackBST<Double, VOPuntoCiudad>();
		Iterable<String> llaves = grafoPorDistancias.keys();
		for (String llave : llaves) {
			VOPuntoCiudad puntoIterable = grafoPorDistancias.getInfoVertex(llave);
			double distancia = distancia(latitud, longitud, puntoIterable.getLatitud(), puntoIterable.getLongitud());
			puntoMasCercano.put(distancia, puntoIterable);
		}
		double llave = puntoMasCercano.min();
		return puntoMasCercano.get(llave);
	}

	private double distanciaVertices(VOPuntoCiudad puntoIterable, VOPuntoCiudad puntoReferencia) {
		double lat2 = puntoIterable.getLatitud(), lng2 = puntoIterable.getLongitud(),
				lat1 = puntoReferencia.getLatitud(), lng1 = puntoIterable.getLongitud();
		double radioTierra = 6371 * 1000;// en metros
		double dLat = Math.toRadians(lat2 - lat1);
		double dLng = Math.toRadians(lng2 - lng1);
		double sindLat = Math.sin(dLat / 2);
		double sindLng = Math.sin(dLng / 2);
		double va1 = Math.pow(sindLat, 2)
				+ Math.pow(sindLng, 2) * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));
		double va2 = 2 * Math.atan2(Math.sqrt(va1), Math.sqrt(1 - va1));
		double distancia = radioTierra * va2;
		return distancia;
	}

	private double distancia(double lat1, double lng1, double lat2, double lng2) {
		double radioTierra = 6371 * 1000;// en metros
		double dLat = Math.toRadians(lat2 - lat1);
		double dLng = Math.toRadians(lng2 - lng1);
		double sindLat = Math.sin(dLat / 2);
		double sindLng = Math.sin(dLng / 2);
		double va1 = Math.pow(sindLat, 2)
				+ Math.pow(sindLng, 2) * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));
		double va2 = 2 * Math.atan2(Math.sqrt(va1), Math.sqrt(1 - va1));
		double distancia = radioTierra * va2;
		return distancia;
	}

}
