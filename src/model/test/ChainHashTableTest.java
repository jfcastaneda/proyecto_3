package model.test;
import model.data_structures.*;

import junit.framework.TestCase;

public class ChainHashTableTest extends TestCase{
	
	private SeparateChaningHasTable<Integer,String> tabla;
	private int capacity = 10;
	
	private void setupScenario1(){
		tabla = new SeparateChaningHasTable<Integer,String>(capacity);
	}
	
	private void setUpScenario2(){
		setupScenario1();
		tabla.put(1, "a");
		tabla.put(2, "b");
		tabla.put(3, "c");
		tabla.put(4, "d");
		tabla.put(5, "e");
		tabla.put(6, "f");
		tabla.put(7, "g");
		tabla.put(8, "h");
		tabla.put(9, "i");
		tabla.put(10, "j");
	}
	
	
	//Test donde se vefica que la tabla halla sido creada correctamente
	public void testSeparateChaningHasTable(){
		setupScenario1();
		LinkedListHash<Integer, String>[] listas = tabla.getArray();
		assertNotNull("La lista de la tabala no puede ser nula", listas);
		for (int i = 0; i < listas.length; i++) {
			assertNotNull("Las listas encadenadas no pueden ser nulas", listas[i]);
		}
	}
	
	public void testPut(){
		setupScenario1();
		tabla.put(1, "a");
		tabla.put(2, "b");
		tabla.put(3, "c");
		tabla.put(4, "d");
		tabla.put(5, "e");
		tabla.put(6, "aa");
		tabla.put(7, "bb");
		tabla.put(8, "cc");
		tabla.put(9, "dd");
		tabla.put(10, "ee");
		assertEquals(10, tabla.getSize());
		LinkedListHash<Integer, String>[] listas = tabla.getArray();
		for (int i = 0; i < listas.length; i++) {
			assertNotNull("Las listas encadenadas no pueden ser nulas", listas[i]);
		}
	}
	
	public void testGet(){
		setUpScenario2();
		assertEquals("a", tabla.get(1));
		assertEquals("b", tabla.get(2));
		assertEquals("c", tabla.get(3));
		assertEquals("d", tabla.get(4));
		assertEquals("e", tabla.get(5));
		assertEquals("f", tabla.get(6));
		assertEquals("g", tabla.get(7));
		assertEquals("h", tabla.get(8));
		assertEquals("i", tabla.get(9));
		assertEquals("j", tabla.get(10));	
	}

	public void testDelete(){
		setUpScenario2();
		assertEquals("a", tabla.delete(1));
		assertEquals("b", tabla.delete(2));
		assertEquals("c", tabla.delete(3));
		assertEquals("d", tabla.delete(4));
		assertEquals("e", tabla.delete(5));
		assertEquals("f", tabla.delete(6));
		assertEquals("g", tabla.delete(7));
		assertEquals("h", tabla.delete(8));
		assertEquals("i", tabla.delete(9));
		assertEquals("j", tabla.delete(10));
	}
}
