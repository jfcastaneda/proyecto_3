package model.test;

import junit.framework.TestCase;
import model.data_structures.*;


public class DirectedGraphTest extends TestCase{
	private DiGraph<Integer, String, Integer> grafoPrueba;

	private void setupScenario1(){
		grafoPrueba = new DiGraph<Integer,String,Integer>();
	}

	private void setupScenario2(){
		setupScenario1();
		grafoPrueba.addVertex(0, "0");
		grafoPrueba.addVertex(1, "1");
		grafoPrueba.addVertex(2, "2");
		grafoPrueba.addVertex(3, "3");
		grafoPrueba.addVertex(4, "4");
	}

	private void setupScenario3(){
		setupScenario2();
		grafoPrueba.addEdge(0, 1, 5);
		grafoPrueba.addEdge(0, 4, 5);
		grafoPrueba.addEdge(0, 2, 3);
		grafoPrueba.addEdge(1, 4, 5);
		grafoPrueba.addEdge(1,3,5);
		grafoPrueba.addEdge(2, 1, 2);
		grafoPrueba.addEdge(2, 4, 1);
		grafoPrueba.addEdge(3, 4, 5);
	}

	public void testDigraph(){
		assertNull("El grafo deberia ser nulo", grafoPrueba);
		setupScenario1();
		assertNotNull("El grafo no deberia ser nulo", grafoPrueba);
	}

	public void testE(){
		setupScenario1();
		assertSame("No deberian haber vertices dentro del grafo", 0, grafoPrueba.E());
		setupScenario3();
		assertSame("Deberian haber vertices dentro del grafo", 8, grafoPrueba.E());
	}


	public void testV(){
		setupScenario1();
		assertSame("No deberian haber vertices dentro del grafo", 0, grafoPrueba.V());
		setupScenario2();
		assertSame("Deberian haber vertices dentro del grafo", 5, grafoPrueba.V());
	}

	public void testAddVertex(){
		setupScenario1();
		grafoPrueba.addVertex(0, "0");
		grafoPrueba.addVertex(1, "1");
		grafoPrueba.addVertex(2, "2");
		grafoPrueba.addVertex(3, "3");
		grafoPrueba.addVertex(4, "4");
		assertSame("Deberian haber vertices dentro del grafo", 5, grafoPrueba.V());


		setupScenario2();
		grafoPrueba.addVertex(0, "5");
		grafoPrueba.addVertex(1, "6");
		grafoPrueba.addVertex(2, "7");
		grafoPrueba.addVertex(3, "8");
		grafoPrueba.addVertex(4, "9");
		assertSame("Deberian haber vertices dentro del grafo", 5, grafoPrueba.V());
		grafoPrueba.addVertex(5, "0");
		grafoPrueba.addVertex(6, "1");
		grafoPrueba.addVertex(7, "2");
		grafoPrueba.addVertex(8, "3");
		grafoPrueba.addVertex(9, "4");
		assertSame("Deberian haber vertices dentro del grafo", 10, grafoPrueba.V());

		try {
			grafoPrueba.addVertex(null, "5");
		} catch (Exception e) {
			assertNotNull("Deberia existir una excepcion", e);
		}

		try {
			grafoPrueba.addVertex(0, null);
		} catch (Exception e) {
			assertNotNull("Deberia existir una excepcion", e);
		}
	}

	public void testAddEdge(){

		setupScenario2();
		grafoPrueba.addEdge(0, 1, 5);
		grafoPrueba.addEdge(0, 4, 5);
		grafoPrueba.addEdge(0, 2, 3);
		grafoPrueba.addEdge(1, 4, 5);
		grafoPrueba.addEdge(1,3,5);
		grafoPrueba.addEdge(2, 1, 2);
		grafoPrueba.addEdge(2, 4, 1);
		grafoPrueba.addEdge(3, 4, 5);
		assertSame("Deberian haber vertices dentro del grafo", 8, grafoPrueba.E());

		grafoPrueba.addEdge(0, 3, 1);
		grafoPrueba.addEdge(4, 0, 1);
		grafoPrueba.addEdge(4, 3, 1);
		grafoPrueba.addEdge(4, 1, 1);
		grafoPrueba.addEdge(4, 2, 1);
		assertSame("Deberian haber vertices dentro del grafo", 13, grafoPrueba.E());

		try {
			grafoPrueba.addEdge(0, 1, null);
		} catch (Exception e) {
			assertNotNull("Deberia existir una excepcion", e);
		}

		try {
			grafoPrueba.addEdge(0, null, 2);
		} catch (Exception e) {
			assertNotNull("Deberia existir una excepcion", e);
		}

		try {
			grafoPrueba.addEdge(null, 0, 2);
		} catch (Exception e) {
			assertNotNull("Deberia existir una excepcion", e);
		}
	}

	public void testgetInfoVertex(){
		setupScenario2();
		assertSame("La info del vertice no es la correcta", "0", grafoPrueba.getInfoVertex(0));
		assertSame("La info del vertice no es la correcta", "1", grafoPrueba.getInfoVertex(1));
		assertSame("La info del vertice no es la correcta", "2", grafoPrueba.getInfoVertex(2));
		assertSame("La info del vertice no es la correcta", "3", grafoPrueba.getInfoVertex(3));
		assertSame("La info del vertice no es la correcta", "4", grafoPrueba.getInfoVertex(4));
		try {
			grafoPrueba.getInfoVertex(100);
		} catch (Exception e) {
			assertNotNull("Deberia existir una excepcion", e);
		}		
	}

	public void testgetInfoEdge(){
		setupScenario3();
		assertSame("La informacion del arco no es la misma",5 , grafoPrueba.getInfoArc(0, 1));
		assertSame("La informacion del arco no es la misma",5 , grafoPrueba.getInfoArc(0, 4));
		assertSame("La informacion del arco no es la misma",3 , grafoPrueba.getInfoArc(0, 2));
		assertSame("La informacion del arco no es la misma",5 , grafoPrueba.getInfoArc(1, 4));
		assertNull("La informacion del arco no es la misma", grafoPrueba.getInfoArc(4, 0));
	}

	public void testsetInfoVertex(){
		setupScenario2();
		grafoPrueba.setInfoVertex(0, "5");
		grafoPrueba.setInfoVertex(1, "6");
		grafoPrueba.setInfoVertex(2, "7");
		grafoPrueba.setInfoVertex(3, "8");
		grafoPrueba.setInfoVertex(4, "9");
		assertSame("La info del vertice no es la correcta", "5", grafoPrueba.getInfoVertex(0));
		assertSame("La info del vertice no es la correcta", "6", grafoPrueba.getInfoVertex(1));
		assertSame("La info del vertice no es la correcta", "7", grafoPrueba.getInfoVertex(2));
		assertSame("La info del vertice no es la correcta", "8", grafoPrueba.getInfoVertex(3));
		assertSame("La info del vertice no es la correcta", "9", grafoPrueba.getInfoVertex(4));
	}

	public void testsetInfoEdge(){
		setupScenario3();
		grafoPrueba.setInforArc(0, 1, 3);
		grafoPrueba.setInforArc(0, 4, 3);
		grafoPrueba.setInforArc(0, 2, 3);
		grafoPrueba.setInforArc(1, 4, 3);
		assertSame("La informacion del arco no es la misma",3 , grafoPrueba.getInfoArc(0, 1));
		assertSame("La informacion del arco no es la misma",3 , grafoPrueba.getInfoArc(0, 4));
		assertSame("La informacion del arco no es la misma",3 , grafoPrueba.getInfoArc(0, 2));
		assertSame("La informacion del arco no es la misma",3 , grafoPrueba.getInfoArc(1, 4));
	}

	public void testAdj(){
		setupScenario3();
		Iterable<Integer> adjacentes = grafoPrueba.adj(0);
		int rta = 0;
		for (Integer integer : adjacentes) {
			rta++;
		}
		assertSame("No se tiene el numero correcto de vertices", rta,3);
		adjacentes = grafoPrueba.adj(1);
		rta=0;
		for (Integer integer : adjacentes) {
			rta++;
		}
		assertSame("No se tiene el numero correcto de vertices", rta,2);
		adjacentes = grafoPrueba.adj(4);
		assertNull("El vertice 4 no tiene verices adjacentes", adjacentes);
	}

	public void testDijkstra(){
		setupScenario3();
		assertNotSame("Si hay un camino entre los vertices", "No hay camino disponible", grafoPrueba.Dijkstra(0, 1));
		assertEquals("0===>2===>1", grafoPrueba.Dijkstra(0, 1));
	}
	
	
}

