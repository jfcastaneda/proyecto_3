package model.test;

import model.comparators.*;
import model.data_structures.Iterador;
import model.data_structures.MaxPQ;

import java.util.UUID;
import java.util.Iterator;
import java.util.NoSuchElementException;
import junit.framework.TestCase;
import java.util.Random;

public class MaxPQTest extends TestCase{

	public static ComparadorStringTest comparador = new ComparadorStringTest();
	public 	String[] keys;
	public MaxPQ<String> MaxHeap;

	private void setupScenario1(){
		MaxHeap= new MaxPQ<String>();
	}

	private void setupScenario2() {
		MaxHeap = new MaxPQ<String>(comparador);
	}

	private void setupScenario3() {
		MaxHeap = new MaxPQ<String>(10,comparador);
	}

	private void setupScenario4() {
		keys = new String[] {"A","C","F","H","Z","@","Q","6","{","<"};
		MaxHeap = new MaxPQ<String>(keys);
	}


	public void testMaxPQ() {
		setupScenario1();
		assertTrue("La cola de prioridad de deberia estas vacia", MaxHeap.isEmpty());
		setupScenario2();
		assertTrue("La cola de prioridad de deberia estas vacia", MaxHeap.isEmpty());
		assertNotNull("El comparador no deberia ser nulo", MaxHeap.getComparator());
		setupScenario3();
		assertTrue("La cola de prioridad de deberia estas vacia", MaxHeap.isEmpty());
		assertNotNull("El comparador no deberia ser nulo", MaxHeap.getComparator());
		setupScenario4();
		assertFalse("La cola de prioridad de deberia estas vacia", MaxHeap.isEmpty());
		assertNull("El comparador deberia ser nulo", MaxHeap.getComparator());
		assertSame("El tama�o de la cola es el incorrecto", keys.length, MaxHeap.size());
	}

	public void testgetMax() {
		setupScenario1();
		try {
			MaxHeap.max();
		}catch (Exception e) {
			assertTrue("No es la excepcion correcta", e instanceof NoSuchElementException);
		}
		setupScenario4();
		String max = "";
		try {
			max = MaxHeap.max();			
		} catch (Exception e) {
			assertNull("No se debeira mandar excepcion",e);
		}finally {
			assertSame("No es el elemento maximo de la cola", "{", max);
		}
	}

	public void testContains() {
		setupScenario1();
		assertFalse("La cola de prioridad no puede contener un elemento estando vacia", MaxHeap.contains("A"));
		setupScenario4();
		assertTrue("La cola de prioridad deberia contener el elemento" + "'40'", MaxHeap.contains("@"));
		assertFalse("La cola de prioridad no deberia contener el elemento" + "'1'", MaxHeap.contains("^"));
	}

	public void testInsert() {
		setupScenario1();
		Random rG = new Random();
		String mayor = "";
		int size = rG.nextInt(20);
		for (int i = 0; i < size; i++) {
			String random = generateString();
			MaxHeap.insert(random);
			mayor = (random.compareTo(mayor)>=0)? random:mayor;
		}
		String max = "";
		try {
			max = MaxHeap.max();
		} catch (Exception e) {
			assertNull("No se debeira mandar excepcion",e);
		}finally {
			assertSame("No es el elemento maximo de la cola", mayor, max);
		}	
	}
	
	public void delMax() {
		setupScenario1();
		Random rG = new Random();
		String mayor = "";
		int size = rG.nextInt(20);
		for (int i = 0; i < size; i++) {
			String random = generateString();
			MaxHeap.insert(random);
			mayor = (random.compareTo(mayor)>=0)? random:mayor;
		}
		String max = "";
		try {
			max = MaxHeap.delMax();
		} catch (Exception e) {
			assertNull("No se debeira mandar excepcion",e);
		}finally {
			assertSame("No es el elemento maximo de la cola", mayor, max);
		}	
		setupScenario4();
		mayor = "{";
		try {
			max = MaxHeap.delMax();
		} catch (Exception e) {
			assertNull("No se debeira mandar excepcion",e);
		}finally {
			assertSame("No es el elemento maximo de la cola", mayor, max);
		}
	}
	


	    private static String generateString() {
	    	String uuid ="";
	        while(uuid.equals("")) {uuid = UUID.randomUUID().toString();}
	        return uuid;
	    }
	}



