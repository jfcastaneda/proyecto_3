package model.test;


import model.comparators.*;

import model.data_structures.ArrayList;
import java.util.Comparator;
import junit.framework.TestCase;
import model.data_structures.RBTree;
import model.data_structures.RBTree.Nodo;


public class TreeTest extends TestCase{

	private RBTree<Integer,String> arbol;

	private void setupScenario1() {
		Comparator comparador = new ComparadorIntTest();
		arbol = new RBTree<Integer, String>(comparador);
	}

	private void setupScenario2() {
		setupScenario1();
		arbol.put(5, "a");
		arbol.put(6,"b");
		arbol.put(4, "c");
		arbol.put(7, "d");
		arbol.put(3, "e");
		arbol.put(8, "f");
		arbol.put(2, "g");
		arbol.put(1, "h");
		arbol.put(9, "i");
	}

	public void testRBTree() {
		setupScenario1();
		assertNull("La raiz deberia ser nula", arbol.getNodoRaiz());
		assertSame("El tama�io deberia ser 0", 0, arbol.getTamanio());
		assertSame("La atura deberia ser 0", 0, arbol.getTamanio());
	}

	public void testPut() {
		setupScenario1();
		arbol.put(5, "a");
		arbol.put(6, "b");
		arbol.put(4, "c");
		arbol.put(7, "d");
		arbol.put(3, "e");
		arbol.put(8, "f");
		arbol.put(2, "g");
		arbol.put(1, "h");
		arbol.put(9, "i");
		Nodo<Integer,String> raiz = arbol.getNodoRaiz();
		assertSame("La raiz no es la correcta", raiz.getValor(),"a");
		assertNotNull("El hijo izquierdo no deberia ser nulo", raiz.getIzquierda());
		assertSame("El hijo izquierdo no es el correcto", raiz.getIzquierda().getValor(),"e");
		assertNotNull("El hijo derecho no deberia ser nulo", raiz.getDerecha());
		assertSame("El hijo izquierdo no es el correcto", raiz.getDerecha().getValor(),"d");
		assertSame("El tama�io no es el correcto", 9,arbol.getTamanio());
		assertSame("La altura no es la correcta", 3,arbol.getAltura() );
	}


	public void testGet() {
		setupScenario2();
		String h = arbol.get(1);
		String g = arbol.get(2);
		String e = arbol.get(3);
		String c = arbol.get(4);
		String a = arbol.get(5);
		String b = arbol.get(6);
		String d = arbol.get(7);
		String f = arbol.get(8);
		String i = arbol.get(9);
		assertSame("El valor no es el esperado", "h", h);
		assertSame("El valor no es el esperado", "g", g);
		assertSame("El valor no es el esperado", "e", e);
		assertSame("El valor no es el esperado", "c", c);
		assertSame("El valor no es el esperado", "a", a);
		assertSame("El valor no es el esperado", "b", b);
		assertSame("El valor no es el esperado", "d", d);
		assertSame("El valor no es el esperado", "f", f);
		assertSame("El valor no es el esperado", "i", i);
	}

	public void testDelete() {
		setupScenario2();
		arbol.delete(1);
		assertSame("El tama�o no es el correcto", 8, arbol.getTamanio());
		arbol.delete(5);
		assertNotNull("La raiz no puede ser nula", arbol.getNodoRaiz());
		assertNotSame("No deberia ser la raiz", "a", arbol.getNodoRaiz().getValor());
		assertNull("No debe existir el objeto", arbol.get(5));
	}

}

