package model.vo;

import model.data_structures.*;


public class VOPuntoCiudad {

	private LinkedList<String> idServiciosCabeza;

	private LinkedList<String> idServiciosCola;
	
	private double latitud;

	private double longitud;

	private String idVertice;

	private boolean visitado;
	
	
	public VOPuntoCiudad(LinkedList<String> idServiciosCabeza, LinkedList<String> idServiciosCola, double latitud,
			double longitud, String idVertice){
		this.idServiciosCabeza = idServiciosCabeza;
		this.idServiciosCola = idServiciosCola;
		this.latitud = latitud;
		this.longitud = longitud;
		this.idVertice = idVertice;
		this.visitado = false;
	}


	public String getIdVertice() {
		return idVertice;
	}

	public LinkedList<String> getIdServiciosCabeza() {
		return idServiciosCabeza;
	}


	public void setIdServiciosCabeza(LinkedList<String> idServiciosCabeza) {
		this.idServiciosCabeza = idServiciosCabeza;
	}


	public LinkedList<String> getIdServiciosCola() {
		return idServiciosCola;
	}


	public void setIdServiciosCola(LinkedList<String> idServiciosCola) {
		this.idServiciosCola = idServiciosCola;
	}


	public void setIdVertice(String idVertice) {
		this.idVertice = idVertice;
	}


	public double getLatitud() {
		return latitud;
	}

	public void setLatitud(double latitud) {
		this.latitud = latitud;
	}

	public double getLongitud() {
		return longitud;
	}

	public void setLongitud(double longitud) {
		this.longitud = longitud;
	}

	
	public void agregarDatosVerticeCercanoCabeza(VOPuntoCiudad punto){
		LinkedList<String> idServiciosPunto = punto.getIdServiciosCabeza();
		for (String string : idServiciosPunto) {
			idServiciosCabeza.add(string);
		}
	}
	
	public void agregarDatosVerticeCercanoCola(VOPuntoCiudad punto){
		LinkedList<String> idServiciosPunto = punto.getIdServiciosCola();
		for (String string : idServiciosPunto) {
			idServiciosCola.add(string);
		}
	}


	public boolean isVisitado() {
		return visitado;
	}


	public void setVisitado(boolean visitado) {
		this.visitado = visitado;
	}

}
