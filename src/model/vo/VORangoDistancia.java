package model.vo;

public class VORangoDistancia {

	private double limiteInferior;

	private double limiteSuperior;
	
	public VORangoDistancia(double limiteInferior, double limiteSuperior){
		
		this.limiteInferior = limiteInferior;
		
		this.limiteSuperior = limiteSuperior;
		
	}

	public double getLimiteInferior() {
		return limiteInferior;
	}

	public void setLimiteInferior(double limiteInferior) {
		this.limiteInferior = limiteInferior;
	}

	public double getLimiteSuperior() {
		return limiteSuperior;
	}

	public void setLimiteSuperior(double limiteSuperior) {
		this.limiteSuperior = limiteSuperior;
	}

}
