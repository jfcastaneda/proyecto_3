package model.vo;

import java.text.SimpleDateFormat;

import model.comparators.ComparatorVOServicioDistancia;
import model.comparators.ComparatorVOServicioDuracion;
import model.comparators.ComparatorVOServicioFechaInicio;

// TODO: Auto-generated Javadoc
/**
 * VO que representa un servicio de taxi realizado en la ciudad de Chicago.
 * @author jf.castaneda@uniandes.edu.co
 * @author md.galvan@uniandes.edu.co
 */
public class VOServicio {

	// ------------------------------------------------------------------------------------
	// ------------------------- Constantes -----------------------------------------------
	// ------------------------------------------------------------------------------------

	/** The Constant FORMAT. */
	public static final SimpleDateFormat FORMAT = new SimpleDateFormat("YYYY-MM-DD'T'HH:mm:ss'.'000");
	
	/** The Constant INICIO_DE_LOS_TIEMPOS. */
	public static final long INICIO_DE_LOS_TIEMPOS = 0;
	
	/** The Constant FIN_DE_LOS_TIEMPOS. */
	public static final long FIN_DE_LOS_TIEMPOS = Long.MAX_VALUE;
	
	/** The Constant COMPARATOR_VOSERVICIO_DURACION. */
	public static final ComparatorVOServicioDuracion COMPARATOR_VOSERVICIO_DURACION = new ComparatorVOServicioDuracion();
	
	/** The Constant COMPARATOR_VOSERVICIO_DISTANCIA. */
	public static final ComparatorVOServicioDistancia COMPARATOR_VOSERVICIO_DISTANCIA = new ComparatorVOServicioDistancia();
	
	/** The Constant COMPARATOR_VOSERVICIO_FECHAINICIO. */
	public static final ComparatorVOServicioFechaInicio COMPARATOR_VOSERVICIO_FECHAINICIO = new ComparatorVOServicioFechaInicio();

	// ------------------------------------------------------------------------------------
	// ------------------------- Atributos ------------------------------------------------
	// ------------------------------------------------------------------------------------

	/** The id servicio. */
	private String idServicio;

	/** The duracion. En segundos.*/
	private int duracionEnSegundos;

	/** The distancia recorrida en millas. */
	private double distanciaRecorridaEnMillas;

	/** The costo total. */
	private double costoTotal;

	/** The fecha inicio. */
	private long fechaInicio;
	
	/** The fecha fin. */
	private long fechaFin;
	
	/** The id zona inicio. */
	private String idZonaInicio;
	
	/** The id zona fin. */
	private String idZonaFin;
	
	/** The taxi. */
	private VOTaxi taxi;
	
	/**
	 *  The llave zonaInicio-ZonaFinal
	 */
	private String llaveZonas;
	
	
	private double latitud;
	
	private double longuitud;
	
	// ------------------------------------------------------------------------------------
	// ------------------------- M�todo Constructor ---------------------------------------
	// ------------------------------------------------------------------------------------

	/**
	 * Instantiates a new VO servicio.
	 *
	 * @param idServicio the id servicio
	 * @param duracionEnSegundos the duracion en segundos
	 * @param distanciaRecorridaEnMillas the distancia recorrida en millas
	 * @param costoTotal the costo total
	 * @param fechaInicio the fecha inicio
	 * @param idZonaInicio the id zona inicio
	 * @param idZonaFin the id zona fin
	 * @param taxi the taxi
	 */
	public VOServicio(String idServicio, int duracionEnSegundos, double distanciaRecorridaEnMillas, double costoTotal,
			long fechaInicio, long fechaFin, String idZonaInicio, String idZonaFin, VOTaxi taxi, double latitud, double longitud) {
		super();
		this.idServicio = idServicio;
		this.duracionEnSegundos = duracionEnSegundos;
		this.distanciaRecorridaEnMillas = distanciaRecorridaEnMillas;
		this.costoTotal = costoTotal;
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
		this.idZonaInicio = idZonaInicio;
		this.idZonaFin = idZonaFin;
		this.taxi = taxi;
		this.llaveZonas = idZonaInicio+"-"+idZonaFin;
		this.latitud = latitud;
		this.longuitud = longitud;
	}

	// ------------------------------------------------------------------------------------
	// ------------------------- M�todos --------------------------------------------------
	// ------------------------------------------------------------------------------------
	
	public double getLatitud() {
		return latitud;
	}

	public void setLatitud(double latitud) {
		this.latitud = latitud;
	}

	public double getLonguitud() {
		return longuitud;
	}

	public void setLonguitud(double longuitud) {
		this.longuitud = longuitud;
	}

	/**
	 * M�todo que retorna si la cantidad de puntos del servicio son v�lidos.
	 *
	 * @return true si s� son v�lidos 
	 */
	public boolean puntosValidos() {
		return duracionEnSegundos > 0 && distanciaRecorridaEnMillas > 0;
	}
	
	/**
	 * Gets the id servicio.
	 *
	 * @return the id servicio
	 */
	public String getIdServicio() {
		return idServicio;
	}

	/**
	 * Gets the duracion en segundos.
	 *
	 * @return the duracion en segundos
	 */
	public int getDuracionEnSegundos() {
		return duracionEnSegundos;
	}

	/**
	 * Gets the distancia recorrida en millas.
	 *
	 * @return the distancia recorrida en millas
	 */
	public double getDistanciaRecorridaEnMillas() {
		return distanciaRecorridaEnMillas;
	}

	/**
	 * Gets the costo total.
	 *
	 * @return the costo total
	 */
	public double getCostoTotal() {
		return costoTotal;
	}

	/**
	 * Gets the fecha inicio.
	 *
	 * @return the fecha inicio
	 */
	public long getFechaInicio() {
		return fechaInicio;
	}
	
	/**
	 * Gets the fecha fin.
	 *
	 * @return the fecha fin
	 */
	public long getFechaFin() {
		return fechaFin;
	}
	
	
	/**
	 * Gets the id zona inicio.
	 *
	 * @return the id zona inicio
	 */
	public String getIdZonaInicio() {
		return idZonaInicio;
	}
	
	/**
	 * Gets the id zona fin.
	 *
	 * @return the id zona fin
	 */
	public String getIdZonaFin() {
		return idZonaFin;
	}

	/**
	 * Gets the taxi.
	 *
	 * @return the taxi
	 */
	public VOTaxi getTaxi() {
		return taxi;
	}
	
	/**
	 * Gets the llave del servicio por zona
	 * @return the llaveZonas
	 */
	public String getLLaveZonas(){
		return llaveZonas;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object servicio) {
		try {
			return this.idServicio.equals(((VOServicio)servicio).idServicio);
		}
		catch(ClassCastException pe) {
			return false;
		}
	}
}
