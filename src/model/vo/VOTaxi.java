package model.vo;

import model.data_structures.LinearProbingHashST;
import model.data_structures.LinkedList;
import model.data_structures.SeparateChainingHashST;
import model.data_structures.SeparateChaningHasTable;
import model.comparators.ComparatorVOTaxiPuntos;

// TODO: Auto-generated Javadoc
/**
 * VO que representa un taxi que labora en la ciudad de Chicago.
 * @author jf.castaneda@uniandes.edu.co
 * @author md.galvan@uniandes.edu.co
 */
public class VOTaxi implements Comparable<VOTaxi> {

	public final static ComparatorVOTaxiPuntos COMPARATOR_VOTAXI_PUNTOS = new ComparatorVOTaxiPuntos();

	// ------------------------------------------------------------------------------------
	// ------------------------- Atributos ------------------------------------------------
	// ------------------------------------------------------------------------------------

	/** The taxi id. */
	private String idTaxi;

	/** The servicios por area. */
	private SeparateChainingHashST<String, LinkedList<VOServicio>> serviciosPorArea;

	//** The servicios por distancia referencia
	private SeparateChainingHashST<VORangoDistancia, LinkedList<VOServicio>> serviciosPorDistancia;

	/** The puntos taxi. */
	private double puntosTaxi;

	private LinkedList<VOServicio> serviciosTotales;

	private double ganancia;

	private double distancia;
	
	private double serviciosValidos;

	// ------------------------------------------------------------------------------------
	// ------------------------- M�todo constructor ---------------------------------------
	// ------------------------------------------------------------------------------------

	/**
	 * Instantiates a new VO taxi.
	 *
	 * @param idTaxi the id taxi
	 */
	public VOTaxi(String idTaxi) {
		super();
		this.idTaxi = idTaxi;
		this.serviciosPorArea = new SeparateChainingHashST<>();
		this.serviciosPorDistancia = new SeparateChainingHashST<>(500);
		this.puntosTaxi = 0;
		this.serviciosTotales = new LinkedList<>();
		this.ganancia=0;
		this.distancia=0;
		this.serviciosValidos=0;
	}

	// ------------------------------------------------------------------------------------
	// ------------------------- M�todos --------------------------------------------------
	// ------------------------------------------------------------------------------------

	public LinkedList<VOServicio> getServiciosTotales() {
		return serviciosTotales;
	}

	public void setServiciosTotales(LinkedList<VOServicio> serviciosTotales) {
		this.serviciosTotales = serviciosTotales;
	}

	/**
	 * Gets the id taxi.
	 *
	 * @return the id taxi
	 */
	public String getIdTaxi() {
		return idTaxi;
	}

	/**
	 * Gets the servicios por area.
	 *
	 * @return the servicios por area
	 */
	public SeparateChainingHashST<String, LinkedList<VOServicio>> getServiciosPorArea() {
		return serviciosPorArea;
	}

	/**
	 * Gets the servicios por distancia.
	 *
	 * @return the servicios por distancia
	 */
	public SeparateChainingHashST<VORangoDistancia, LinkedList<VOServicio>> getServiciosPorDistancia() {
		return serviciosPorDistancia;
	}

	/**
	 * Gets the puntos taxi.
	 *
	 * @return the puntos taxi
	 */
	public double getPuntosTaxi() {
		return puntosTaxi;
	}

	public boolean equals(Object taxi) {
		try	{
			return this.idTaxi.equals(((VOTaxi)taxi).idTaxi);
		}
		catch(ClassCastException pe) {
			return false;
		}
	}

	public void agregarServicioATablaDistancia(VORangoDistancia llave, VOServicio servicio){
		serviciosPorDistancia.get(llave).add(servicio);
	}

	public void agregarServicio(VOServicio servicio){
		serviciosTotales.add(servicio);
		if(servicio.getCostoTotal()>0 && servicio.getDistanciaRecorridaEnMillas()>0){
			ganancia+=servicio.getCostoTotal();
			distancia+=servicio.getDistanciaRecorridaEnMillas();
			serviciosValidos+=1;
		}
		calcularPuntos();
	}
	
	private void calcularPuntos(){
		if(distancia!=0) puntosTaxi = (ganancia*serviciosValidos)/distancia;
		else puntosTaxi=-1;
	}


	@Override
	public int compareTo(VOTaxi o) {
		// TODO Auto-generated method stub
		return idTaxi.compareTo(o.idTaxi);
	}
}
