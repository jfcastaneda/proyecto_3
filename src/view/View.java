package view;

import java.io.IOException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Iterator;
import java.util.Scanner;


import controller.Controller;
import model.logic.Manager;
import model.template.TemplateModifier;
import model.data_structures.*;
import model.vo.*;

/**
 * Vista de la aplicaci�n.
 * 
 * @author jf.castaneda@uniandes.edu.co
 * @author md.galvan@uniandes.edu.co
 */
public class View {

	// ------------------------------------------------------------------------------------
	// ------------------------- M�todo Main
	// ----------------------------------------------
	// ------------------------------------------------------------------------------------

	/**
	 * M�todo main de la aplicaci�n.
	 */
	public static void main(String[] args) {


		// Boolean que representa el estado del programa.
		boolean activo = true;

		Scanner sc = new Scanner(System.in);

		// Ciclo del programa mientras est� activo.
		while (activo) {

			// Ejecuci�n de la interfaz principal del men�.
			menu();


			// Impresi�n del men� cuando se cargaron los datos.
			menuConDatos();

			// Lectura de la opci�n que n�mero ingres�.
			int opcion = sc.nextInt();

			// Switch para saber qu� json eligi�.
			switch (opcion) {

			// El usuario eligi� la opci�n de salir.
			case 1:
				System.out.println("Hasta luego.");
				activo = false;
				sc.close();
				break;

				// El usuario eligi� la opci�n de cargar un nuevo archivo.
			case 2:
				// Memoria y tiempo antes de cargar el archivo.
				long memoriaInicio = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				long tiempoInicio = System.nanoTime();
				try {
					Controller.cargarJson();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					System.out.println(e.getMessage());
				}

				// Tiempo que se demor� cargando los datos.
				long tiempoFin = System.nanoTime();
				long duracion = (tiempoFin - tiempoInicio) / (1000000);

				// Memoria usada cargando los datos.
				long memoriaFin = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();

				// Mensaje de cu�nto tiempo se gast� durante el m�todo y
				// cu�nta memoria gast�.
				System.out.println("Tiempo que demor� en cargar: " + duracion
						+ " milisegundos \nMemoria utilizada durante la carga:  "
						+ ((memoriaFin - memoriaInicio) / 1000000.0) + " MB");
				break;
			case 3:
				VOPuntoCiudad VerticeMasCongestionado = Controller.darVerticeMasCongestionado();
				System.out.println("Latitud: " +VerticeMasCongestionado.getLatitud());
				System.out.println("Longitud: " +VerticeMasCongestionado.getLongitud());
				System.out.println("Total de servicios: " + (VerticeMasCongestionado.getIdServiciosCabeza().size()+VerticeMasCongestionado.getIdServiciosCola().size()));
				System.out.println("Servicios que comienzan en el vertice: ");
				System.out.println("�Desea imprimir los servicios? (Y/N)");
				String op = sc.next();
				if(op.equals("Y")){
					LinkedList<String> serCa = VerticeMasCongestionado.getIdServiciosCabeza();
					for (String string : serCa) {
						System.out.println(string);
					}
					System.out.println("Servicios que terminan en el vertice: ");
					LinkedList<String> serCola = VerticeMasCongestionado.getIdServiciosCola();
					for (String string : serCola) {
						System.out.println(string);
					}
				}
				else if(op.equals("N")){
					break;					
				}else{
					System.out.println("Ingrese una opcion valida y vuelva a ejecutar el requerimiento");
					break;
				}
			case 4:
				break;
			case 5:
				break;
			case 6:
				Controller.r4();
				break;
			case 7:
				Controller.r5();
				break;
			case 8:
				Controller.r6();
				break;
			default:
				opcion = -1;
				System.out.println("Ingres� un n�mero inv�lido.");
				break;
			}
		}
	}

	// ------------------------------------------------------------------------------------
	// ------------------------- M�todos
	// --------------------------------------------------
	// ------------------------------------------------------------------------------------

	/**
	 * M�todo que imprime el men� de la aplicaci�n
	 */
	private static void menu() {
		System.out.println("------------------------------------------------------------------------------------");
		System.out.println("------------------------- Taxis de Chicago. Versi�n 2.0 ----------------------------");
		System.out.println("------------------------------------------------------------------------------------");
		System.out.println("");
	}

	/**
	 * M�todo que imprime el resto del men� si la aplicaci�n tiene datos
	 * cargados.
	 */
	private static void menuConDatos() {
		System.out.println("1. Salir de la app.");
		System.out.println("2. Cargar nuevo archivo.");
		System.out.println("3. Dar vertice mas congestionado");
		System.out.println("4. Dar vertices fuertemente conexos");
		System.out.println("5. Generar mapa");
		System.out.println("6. Encontrar camino mas corto entre dos puntos");
		System.out.println("7. Dar caminos de mayor y menor duracion entre dos puntos"
				+ "de menor duracion");
		System.out.println("8. Dar todos los caminos posibles sin pagar peaje entre dos puntos");
	}


	/**
	 * M�todo que imprime las distancias disponibles
	 * cargados.
	 */
	private static void menuDistancias() {
		System.out.println("Seleccione la distancia Dx para crear el grafo [Estas distancias estan en metros]:");
		System.out.println("1. 25");
		System.out.println("2. 50");
		System.out.println("3. 75");
		System.out.println("4. 100");
		System.out.println("5. 250");
		System.out.println("6. 500");
		System.out.println("7. 750");
	}
}
